package package1;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CustomerService{
    CustomerDAO dao;
    AccountService accountService;

    @PostConstruct
    public void init() {
        save(new Customer("Євгеній", "uihe@gmail.com", 25));
        save(new Customer("Оксана", "gp@gmail.com", 24));
        save(new Customer("Павло", "liaheg@gmail.com", 20));
        save(new Customer("Олег", "uiafe@gmail.com", 30));
        save(new Customer("Марія", "aegaeg@gmail.com", 50));

        addAcc(1L);
        addAcc(1L);
        addAcc(2L);
        addAcc(2L);
        addAcc(3L);
        addAcc(4L);
        addAcc(5L);
    }
    public Customer save(Customer obj) {
        return dao.save(obj);
    }

    public boolean delete(Customer obj) {
        return dao.delete(obj);
    }


    public void deleteAll(ArrayList<Customer> entities) {
        dao.deleteAll(entities);
    }


    public void saveAll(List<Customer> entities) {
        dao.saveAll(entities);
    }


    public ArrayList<Customer> findAll() {
        return dao.findAll();
    }


    public boolean deleteById(Long id) {
        return dao.deleteById(id);
    }

    public Customer getOne(Long id) {
        return dao.getOne(id);
    }

    public Customer editById(Long id, String name, String email, Integer age){
        dao.getOne(id).setAge(age);
        dao.getOne(id).setName(name);
        dao.getOne(id).setEmail(email);
        return dao.getOne(id);
    }

    public Customer addAcc(Long id) {
        Customer customer = dao.getOne(id);

        if (customer!=null) {
            Account newAccount = accountService.save(new Account(Currency.UAH, customer)); // UAH???
            customer.getAccounts().add(newAccount);

            return dao.save(customer);
        } else {
            return null;
        }
    }

    public boolean deleteAcc(Long cid, Long aid){
        Customer customer = dao.getOne(cid);

        if (customer!=null) {
            if (customer.getAccounts()!=null)
                customer.getAccounts().stream()
                        .filter(x -> x.getId().equals(aid))
                        .findFirst()
                        .ifPresent(x ->{
                                    customer.getAccounts().remove(x);
                                    accountService.delete(x);
                                }
                        );
            return true;
        } else {
            return false;
        }
    }
}
