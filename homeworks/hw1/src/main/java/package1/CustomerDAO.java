package package1;


import jakarta.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import package1.Customer;
import package1.DAO;

import java.util.ArrayList;
import java.util.List;

@Repository
@NoArgsConstructor
public class CustomerDAO implements DAO<Customer> {
    ArrayList<Customer> customers;
    Long counter;

    @PostConstruct
    public void init() {
        this.customers = new ArrayList<>();
        this.counter = 1L;
    }

    @Override
    public Customer save(Customer obj) {
        if (obj.getId() == null) {
            obj.setId(counter);
            counter += 1;
            customers.add(obj);
        } else {
            for (int i = 0; i < customers.size(); i++) {
                if (customers.get(i).getId().equals(obj.getId())) {
                    customers.set(i, obj);
                    break;
                }
            }
        }
        return obj;
    }

    @Override
    public boolean delete(Customer obj) {
        if(customers.contains(obj)) {
            customers.remove(obj);
            return true;
        }
        else return false;
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        for(Customer entity : entities){
            delete(entity);
        }
    }

    @Override
    public void saveAll(List<Customer> entities) {
        for(Customer entity : entities){
            save(entity);
        }
    }

    @Override
    public ArrayList<Customer> findAll() {
        return customers;
    }

    @Override
    public boolean deleteById(Long id) {
        Customer customer = customers.stream()
                .filter(x -> x.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (customer != null) {
            customers.remove(customer);
            return true;
        } else return false;
    }

    @Override
    public Customer getOne(Long id) {
        return customers.stream()
                .filter(x -> x.getId().equals(id))
                .findFirst().orElse(null);
    }
}
