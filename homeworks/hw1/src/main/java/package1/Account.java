package package1;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import package1.Currency;
import package1.Customer;

import java.util.UUID;

@Getter
@Setter
@ToString
public class Account {
    Long id;
    String number;
    Currency currency;
    Double balance;
    Customer customer;

    Account(Currency currency, Customer customer){
        this.number = UUID.randomUUID().toString();
        this.currency = currency;
        this.balance = 0.0;
        this.customer = customer;
    }
}
