package package1;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployerDAO implements DAO<Employer> {
    private final EmployerRepository repository;

    public EmployerDAO(EmployerRepository repository) {
        this.repository = repository;
    }
    @Override
    public Employer save(Employer obj) {
        return repository.save(obj);
    }

    @Override
    public boolean delete(Employer obj) {
        if(repository.findAll().contains(obj)) {
            repository.delete(obj);
            return true;
        }
        else return false;
    }

    @Override
    public void deleteAll(List<Employer> entities) {
        repository.deleteAll();
    }

    @Override
    public void saveAll(List<Employer> entities) {
        repository.saveAll(entities);
    }

    @Override
    public ArrayList<Employer> findAll() {
        return (ArrayList<Employer>) repository.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        repository.deleteById(id);
        return true;
    }

    @Override
    public Employer getOne(Long id) {
        return repository.findById(id).orElse(null);
    }
}