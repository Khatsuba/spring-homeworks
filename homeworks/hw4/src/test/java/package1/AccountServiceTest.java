package package1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import package1.Account;
import package1.AccountDAO;
import package1.AccountService;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class AccountServiceTest {

    @MockBean
    private AccountDAO accountDAO;

    @Autowired
    private AccountService accountService;

    @Test
    public void testSave() {

        Account account = new Account();
        account.setId(1L);

        when(accountDAO.save(account)).thenReturn(account);

        Account result = accountService.save(account);

        assertEquals(account, result);
    }

    @Test
    public void testDelete() {
        Account account = new Account();
        when(accountDAO.delete(account)).thenReturn(true);

        boolean result = accountService.delete(account);

        verify(accountDAO, times(1)).delete(account);
        assertTrue(result);
    }

    @Test
    public void testFindAll() {
        Account account1 = new Account();
        account1.setId(1L);
        Account account2 = new Account();
        account2.setId(2L);
        ArrayList<Account> accounts = new ArrayList<>(Arrays.asList(account1, account2));

        when(accountDAO.findAll()).thenReturn(accounts);

        ArrayList<Account> result = accountService.findAll();

        verify(accountDAO, times(1)).findAll();
        assertEquals(2, result.size());
        assertTrue(result.contains(account1));
        assertTrue(result.contains(account2));
    }

    @Test
    public void testDeleteById() {
        when(accountDAO.deleteById(1L)).thenReturn(true);

        boolean result = accountService.deleteById(1L);

        verify(accountDAO, times(1)).deleteById(1L);
        assertTrue(result);
    }

    @Test
    public void testGetOne() {
        Account account = new Account();
        account.setId(1L);

        when(accountDAO.getOne(1L)).thenReturn(account);

        Account result = accountService.getOne(1L);

        verify(accountDAO, times(1)).getOne(1L);
        assertEquals(result, account);
    }

    @Test
    public void testGetIdByNumber() {
        Account account1 = new Account();
        account1.setId(1L);
        account1.setNumber("wrong");
        Account account2 = new Account();
        account2.setId(2L);
        account2.setNumber("number");
        ArrayList<Account> accounts = new ArrayList<>(Arrays.asList(account1, account2));

        when(accountDAO.findAll()).thenReturn(accounts);

        Long result = accountService.getIdByNumber("number");

        verify(accountDAO, times(1)).findAll();
        assertEquals(result, account2.getId());
    }

    @Test
    public void testDeposit() {
        Account account = new Account();
        account.setId(1L);
        account.setBalance(100.0);

        when(accountDAO.getOne(1L)).thenReturn(account);

        accountService.deposit(1L, 50.0);

        verify(accountDAO).save(account);
        assertEquals(150.0, account.getBalance());
    }

    @Test
    public void testWithdrawGood() {
        Account account = new Account();
        account.setId(1L);
        account.setBalance(100.0);

        when(accountDAO.getOne(1L)).thenReturn(account);

        boolean result = accountService.withdraw(1L, 50.0);

        verify(accountDAO).save(account);
        assertTrue(result);
        assertEquals(50.0, account.getBalance());
    }

    @Test
    public void testWithdrawBad() {
        Account account = new Account();
        account.setId(1L);
        account.setBalance(100.0);

        when(accountDAO.getOne(1L)).thenReturn(account);

        boolean result = accountService.withdraw(1L, 150.0);

        verify(accountDAO, never()).save(account);
        assertFalse(result);
        assertEquals(100.0, account.getBalance());
    }

    @Test
    public void testTransferGood() {
        Account from = new Account();
        from.setId(1L);
        from.setBalance(100.0);

        Account to = new Account();
        to.setId(2L);
        to.setBalance(0.0);

        when(accountDAO.getOne(1L)).thenReturn(from);
        when(accountDAO.getOne(2L)).thenReturn(to);

        boolean result = accountService.transfer(1L, 2L, 50.0);

        verify(accountDAO, times(2)).save(any(Account.class));
        assertTrue(result);
        assertEquals(50.0, from.getBalance());
        assertEquals(50.0, to.getBalance());
    }

    @Test
    public void testTransferBad() {
        Account from = new Account();
        from.setId(1L);
        from.setBalance(100.0);

        Account to = new Account();
        to.setId(2L);
        to.setBalance(0.0);

        when(accountDAO.getOne(1L)).thenReturn(from);
        when(accountDAO.getOne(2L)).thenReturn(to);

        boolean result = accountService.transfer(1L, 2L, 150.0);

        verify(accountDAO, never()).save(from);
        verify(accountDAO, never()).save(to);
        assertFalse(result);
        assertEquals(100.0, from.getBalance());
        assertEquals(0.0, to.getBalance());
    }
}