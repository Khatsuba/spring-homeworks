package package1;

import jakarta.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import package1.Account;
import package1.DAO;

import java.util.ArrayList;
import java.util.List;

@Repository
@NoArgsConstructor
public class AccountDAO implements DAO<Account> {
    ArrayList<Account> accounts;
    Long counter;

    @PostConstruct
    public void init() {
        this.accounts = new ArrayList<>();
        this.counter= 1L;
    }

    @Override
    public Account save(Account obj) {
        if (obj.getId() == null) {
            obj.setId(counter);
            counter += 1;
            accounts.add(obj);
        } else {
            for (int i = 0; i < accounts.size(); i++) {
                if (accounts.get(i).getId().equals(obj.getId())) {
                    accounts.set(i, obj);
                    break;
                }
            }
        }
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if(accounts.contains(obj)) {
            accounts.remove(obj);
            return true;
        }
        else return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        for(Account entity : entities){
            delete(entity);
        }
    }

    @Override
    public void saveAll(List<Account> entities) {
        for(Account entity : entities){
            save(entity);
        }
    }

    @Override
    public ArrayList<Account> findAll() {
        return accounts;
    }

    @Override
    public boolean deleteById(Long id) {
        Account account = accounts.stream()
                .filter(x -> x.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (account != null) {
            accounts.remove(account);
            return true;
        } else return false;
    }

    @Override
    public Account getOne(Long id) {
        return accounts.stream()
                .filter(x -> x.getId().equals(id))
                .findFirst().orElse(null);
    }
}