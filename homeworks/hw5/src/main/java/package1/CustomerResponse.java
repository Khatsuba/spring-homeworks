package package1;

import lombok.Data;

import java.util.List;

@Data
public class CustomerResponse {
    Long id;
    String name;
    String email;
    String phone;
    Integer age;
    List<AccountResponse> accounts;
}
