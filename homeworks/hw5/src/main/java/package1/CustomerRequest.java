package package1;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class CustomerRequest {
    @NotNull(message = "Name is expected")
    @Size(min = 2, message = "Name should be at least 2 caracters long")
    String name;

    @NotNull(message = "Email is expected")
    @Pattern(
            regexp = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$",
            message = "Email must be in the right format"
    )
    String email;

    @NotNull(message = "Age is expected")
    @Min(value = 18, message = "Age should be at least 18")
    Integer age;

    @NotNull(message = "Phone is expected")
    @Pattern(
            regexp = "^\\+?[0-9\\s-]{10,15}$",
            message = "Phone number must be in the right format"
    )
    String phone;

    List<Long> employersIds;
}
