insert into abstract_entity (id) values (1);
insert into abstract_entity (id) values (2);
insert into abstract_entity (id) values (3);
insert into abstract_entity (id) values (4);
insert into abstract_entity (id) values (5);
insert into abstract_entity (id) values (6);
insert into abstract_entity (id) values (7);
insert into abstract_entity (id) values (8);
insert into abstract_entity (id) values (9);
insert into abstract_entity (id) values (10);

alter sequence abstract_entity_seq restart with 60;

insert into customer (id, name, email, age, phone, password) values (1, 'Андрій Іваненко', 'andriy.ivanenko@example.com', 30, '+38 067 523 3422', '$2a$10$kRC7jUIrTbbtYLMFjiIQVu4sb9PH3ddFFX87uBoZHdF1UbPS6FDGG');
insert into customer (id, name, email, age, phone, password) values (2, 'Богдан Коваленко', 'bogdan.kovalenko@example.com', 45, '+38 067 542 3342', '$2a$10$kRC7jUIrTbbtYLMFjiIQVu4sb9PH3ddFFX87uBoZHdF1UbPS6FDGG');
insert into customer (id, name, email, age, phone, password) values (3, 'Вікторія Шевченко', 'viktoriya.shevchenko@example.com', 25, '+38 067 502 2334', '$2a$10$kRC7jUIrTbbtYLMFjiIQVu4sb9PH3ddFFX87uBoZHdF1UbPS6FDGG');

insert into account (balance, currency, number, id, customer_id) values (500, 2, 'be1783a8-9df6-4e3b-b17b-30c310b0b040', 4, 1);
insert into account (balance, currency, number, id, customer_id) values (1000, 2, 'be1783a8-9df6-4e3b-b17b-30c310b0b040', 5, 1);
insert into account (balance, currency, number, id, customer_id) values (200, 2, 'be1783a8-9df6-4e3b-b17b-30c310b0b040', 6, 2);
insert into account (balance, currency, number, id, customer_id) values (5000, 2, 'be1783a8-9df6-4e3b-b17b-30c310b0b040', 7, 3);

insert into employer (address, name, id) values ('Маунтін-В''ю, Каліфорнія, США', 'Google', 8);
insert into employer (address, name, id) values ('Сеул, Південна Корея', 'Samsung Electronics', 9);
insert into employer (address, name, id) values ('Сіетл, Вашингтон, США', 'Amazon.com', 10);

insert into customer_accounts (customer_id, accounts_id) values (1, 4);
insert into customer_accounts (customer_id, accounts_id) values (1, 5);
insert into customer_accounts (customer_id, accounts_id) values (2, 6);
insert into customer_accounts (customer_id, accounts_id) values (3, 7);

insert into customer_employers (customer_id, employer_id) values (1, 8);
insert into customer_employers (customer_id, employer_id) values (1, 9);
insert into customer_employers (customer_id, employer_id) values (2, 8);
insert into customer_employers (customer_id, employer_id) values (3, 9);
