package package1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class EmployerServiceTest {

    @MockBean
    private EmployerDAO employerDAO;

    @Autowired
    private EmployerService employerService;

    @Test
    public void testSave() {
        Employer employer = new Employer();
        when(employerDAO.save(employer)).thenReturn(employer);

        Employer result = employerService.save(employer);

        verify(employerDAO, times(1)).save(employer);
        assertEquals(employer, result);
    }


    @Test
    public void testDelete() {
        Employer employer = new Employer();

        when(employerDAO.delete(employer)).thenReturn(true);

        boolean result = employerService.delete(employer);

        assertTrue(result);
        verify(employerDAO, times(1)).delete(employer);
    }

    @Test
    public void testFindAll() {
        Employer employer1 = new Employer();
        employer1.setId(1L);
        Employer employer2 = new Employer();
        employer2.setId(2L);
        ArrayList<Employer> employers = new ArrayList<>(Arrays.asList(employer1, employer2));

        when(employerDAO.findAll()).thenReturn(employers);

        ArrayList<Employer> result = employerService.findAll();

        verify(employerDAO, times(1)).findAll();
        assertEquals(2, result.size());
        assertTrue(result.contains(employer1));
        assertTrue(result.contains(employer2));
    }

    @Test
    public void testDeleteById() {
        when(employerDAO.deleteById(1L)).thenReturn(true);

        boolean result = employerService.deleteById(1L);

        assertTrue(result);
        verify(employerDAO, times(1)).deleteById(1L);
    }

    @Test
    public void testGetOne() {
        Employer employer = new Employer();

        when(employerDAO.getOne(1L)).thenReturn(employer);

        Employer result = employerService.getOne(1L);

        assertEquals(result, employer);
    }

    @Test
    public void testEditById() {
        Employer employer = new Employer();

        when(employerDAO.getOne(1L)).thenReturn(employer);

        employerService.editById(1L, "name", "address");

        verify(employerDAO, times(5)).getOne(1L);
        assertEquals(employer.getName(), "name");
        assertEquals(employer.getAddress(), "address");
    }
}
