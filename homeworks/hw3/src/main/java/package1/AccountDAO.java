package package1;

import jakarta.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import package1.Account;
import package1.DAO;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDAO implements DAO<Account> {
    private final AccountRepository repository;

    public AccountDAO(AccountRepository repository) {
        this.repository = repository;
    }
    @Override
    public Account save(Account obj) {
        repository.save(obj);
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if(repository.findAll().contains(obj)) {
            repository.delete(obj);
            return true;
        }
        else return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        repository.deleteAll();
    }

    @Override
    public void saveAll(List<Account> entities) {
        saveAll(entities);
    }

    @Override
    public ArrayList<Account> findAll() {
        return (ArrayList<Account>) repository.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        repository.deleteById(id);
        return true;
    }

    @Override
    public Account getOne(Long id) {
        return repository.findById(id).orElse(null);
    }
}