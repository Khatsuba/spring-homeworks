package package1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
public class EmployerController {
    private final EmployerService employerService;
    @GetMapping
    public ModelAndView viewAllEmployers() {
        ArrayList<Employer> employers = employerService.findAll();
        ModelAndView modelAndView = new ModelAndView("employers");
        modelAndView.addObject("employers", employers);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView editEmployer(@PathVariable("id") Long id) {
        Employer employer = employerService.getOne(id);
        ModelAndView modelAndView = new ModelAndView("employer-form");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView newEmployer() {
        Employer employer = new Employer();
        ModelAndView modelAndView = new ModelAndView("new-employer");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @PostMapping("/new")
    public ResponseEntity<Object> save(@ModelAttribute Employer employer) {
        employerService.save(employer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @RequestParam String name, @RequestParam String address) {
        Employer employer = employerService.getOne(id);
        employer.setName(name);
        employer.setAddress(address);
        employerService.save(employer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
        employerService.deleteById(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }


}
