package package1;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Customer {
    Long id;
    String name;
    String email;
    Integer age;
    ArrayList<Account> accounts = new ArrayList<>();

    Customer(String name, String email, Integer age){
        this.name = name;
        this.email = email;
        this.age = age;
    }

}
