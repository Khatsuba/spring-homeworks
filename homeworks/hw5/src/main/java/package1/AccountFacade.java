package package1;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AccountFacade {
    private final AccountService accountService;
    private final ModelMapper modelMapper;

    public AccountFacade(AccountService accountService) {
        this.accountService = accountService;
        this.modelMapper = new ModelMapper();
    }

    public void save(AccountRequest obj) {
        Account acc = modelMapper.map(obj, Account.class);
        accountService.save(acc);
    }
    public boolean delete(AccountRequest obj) {
        Account acc = modelMapper.map(obj, Account.class);
        return accountService.delete(acc);
    }
    public ArrayList<AccountResponse> findAll() {
        ArrayList<Account> accounts = accountService.findAll();
        return accounts.stream()
                .map(account -> modelMapper.map(account, AccountResponse.class))
                .collect(Collectors.toCollection(ArrayList::new));
    }
    public AccountResponse getOne(Long id) {
        Account acc = accountService.getOne(id);
        return modelMapper.map(acc, AccountResponse.class);
    }

    public Long getIdByNumber(String number){
        return accountService.getIdByNumber(number);
    }
    public void deposit(Long id, Double amount){
        accountService.deposit(id, amount);
    }
    public boolean withdraw(Long id, Double amount){
        return accountService.withdraw(id, amount);
    }
    public boolean transfer(Long from, Long to, Double amount) {
        return accountService.transfer(from, to, amount);
    }
}
