package package1;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table
@NoArgsConstructor
public class Employer extends AbstractEntity{
    @Column
    String name;
    @Column
    String address;
    @ManyToMany(mappedBy = "employers")
    List<Customer> customers = new ArrayList<>();
}
