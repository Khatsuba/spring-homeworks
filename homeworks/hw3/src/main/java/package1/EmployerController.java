package package1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
public class EmployerController {
    private final EmployerFacade employerFacade;
    @GetMapping
    public ModelAndView viewAllEmployers() {
        ArrayList<EmployerResponse> employers = employerFacade.findAll();
        ModelAndView modelAndView = new ModelAndView("employers");
        modelAndView.addObject("employers", employers);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView editEmployer(@PathVariable("id") Long id) {
        EmployerResponse employer = employerFacade.getOne(id);
        ModelAndView modelAndView = new ModelAndView("employer-form");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView newEmployer() {
        EmployerRequest employer = new EmployerRequest();
        ModelAndView modelAndView = new ModelAndView("new-employer");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @PostMapping("/new")
    public ResponseEntity<Object> save(@Validated @ModelAttribute EmployerRequest employer) {
        employerFacade.save(employer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @Validated @ModelAttribute EmployerRequest employerRequest) {
        employerFacade.updateEmployer(id, employerRequest);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
        employerFacade.deleteById(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }


}
