package package1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployerFacade employerFacade;

    ModelMapper mapper = new ModelMapper();

    private EmployerRequest employerRequest;
    private EmployerResponse employerResponse;
    private Long employerId;

    @BeforeEach
    public void setUp() {
        employerId = 1L;
        employerRequest = new EmployerRequest();
        employerResponse = new EmployerResponse();
    }

    @Test
    public void testViewAllEmployers() throws Exception {
        ArrayList<EmployerResponse> employers = new ArrayList<>();

        when(employerFacade.findAll()).thenReturn(employers);

        mockMvc.perform(MockMvcRequestBuilders.get("/employers"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("employers"))
                .andExpect(MockMvcResultMatchers.model().attribute("employers", employers));

        verify(employerFacade, times(1)).findAll();
    }
    @Test
    public void testEditEmployerGet() throws Exception {
        when(employerFacade.getOne(employerId)).thenReturn(employerResponse);

        mockMvc.perform(MockMvcRequestBuilders.get("/employers/{id}", employerId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("employer-form"))
                .andExpect(MockMvcResultMatchers.model().attribute("employer", employerResponse));

        verify(employerFacade, times(1)).getOne(employerId);
    }

    @Test
    public void testNewEmployer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employers/new"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("new-employer"));
    }

    @Test
    public void testSaveEmployer() throws Exception {
        EmployerRequest newEmployer = new EmployerRequest();
        newEmployer.setName("Name Surname");
        newEmployer.setAddress("123 Address");

        mockMvc.perform(MockMvcRequestBuilders.post("/employers/new")
                        .param("name", newEmployer.getName())
                        .param("address", newEmployer.getAddress()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/employers"));

        verify(employerFacade, times(1)).save(newEmployer);
    }

    @Test
    public void testEditEmployerPost() throws Exception {
        EmployerRequest employer = new EmployerRequest();
        employer.setName("Name Surname");
        employer.setAddress("123 Address");
        employer.setId(1L);

        mockMvc.perform(MockMvcRequestBuilders.post("/employers/{id}/edit", employerId)
                        .param("name", employer.getName())
                        .param("address", employer.getAddress()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/employers"));

        verify(employerFacade, times(1)).updateEmployer(employerId, employer);
    }

    @Test
    public void testDeleteById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/employers/{id}/delete", employerId))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/employers"));

        verify(employerFacade, times(1)).deleteById(employerId);
    }
}