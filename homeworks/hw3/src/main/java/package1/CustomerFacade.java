package package1;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerFacade {
    private final CustomerService customerService;
    private final ModelMapper modelMapper;

    public CustomerFacade(CustomerService customerService) {
        this.customerService = customerService;
        this.modelMapper = new ModelMapper();
    }

    public void save(CustomerRequest obj) {
        customerService.save(modelMapper.map(obj, Customer.class));
    }

    public boolean delete(CustomerRequest obj) {
        return customerService.delete(modelMapper.map(obj, Customer.class));
    }

    public Page<CustomerResponse> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return customerService.findAll(pageable)
                .map(customer -> modelMapper.map(customer, CustomerResponse.class));

    }


    public boolean deleteById(Long id) {
        return customerService.deleteById(id);
    }

    public CustomerResponse getOne(Long id) {
        Customer customer = customerService.getOne(id);
        return modelMapper.map(customer, CustomerResponse.class);
    }

    public List<EmployerResponse> getEmployers(Long id) {
        Customer customer = customerService.getOne(id);
        return customer.getEmployers().stream().map(x -> modelMapper.map(x, EmployerResponse.class)).toList();
    }

    public void updateCustomer(Long id, CustomerRequest request){
        customerService.editById(id, request.getName(), request.getEmail(), request.getAge(), request.getPhone());
        Customer customer = customerService.getOne(id);
        customerService.clearEmployers(id);
        request.getEmployersIds().forEach(x -> customerService.addEmployer(id, x));
        customerService.save(customer);
    }

    public void addAcc(Long id) {
        customerService.addAcc(id);
    }

    public boolean deleteAcc(Long cid, Long aid){
        return customerService.deleteAcc(cid, aid);
    }
}
