package package1;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class EmployerRequest {
    private Long id;

    @NotNull(message = "Name is expected")
    @Size(min = 3, message = "Name should be at least 3 characters")
    private String name;

    @NotNull(message = "Name is expected")
    @Size(min = 3, message = "Address should be at least 3 characters")
    private String address;
}
