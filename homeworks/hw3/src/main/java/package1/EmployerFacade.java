package package1;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployerFacade {
    private final EmployerService employerService;
    private final ModelMapper modelMapper;

    public EmployerFacade(EmployerService employerService) {
        this.employerService = employerService;
        this.modelMapper = new ModelMapper();
    }

    public Employer save(EmployerRequest obj) {
        return employerService.save(modelMapper.map(obj, Employer.class));
    }

    public boolean delete(EmployerRequest obj) {
        return employerService.delete(modelMapper.map(obj, Employer.class));
    }


    public ArrayList<EmployerResponse> findAll() {
        ArrayList<Employer> employers = employerService.findAll();
        return employers.stream()
                .map(employer -> modelMapper.map(employer, EmployerResponse.class))
                .collect(Collectors.toCollection(ArrayList::new));
    }


    public boolean deleteById(Long id) {
        return employerService.deleteById(id);
    }

    public EmployerResponse getOne(Long id) {
        return modelMapper.map(employerService.getOne(id), EmployerResponse.class);
    }

    public void updateEmployer(Long id, EmployerRequest request){
        Employer employer = employerService.getOne(id);
        employer.setAddress(request.getAddress());
        employer.setName(request.getName());
        employerService.save(employer);
    }
}
