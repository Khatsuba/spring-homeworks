package package1;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class AccountService {
    AccountDAO dao;
    public Account save(Account obj) {
        return dao.save(obj);
    }
    public boolean delete(Account obj) {
        return dao.delete(obj);
    }
    public void deleteAll(ArrayList<Account> entities) {
        dao.deleteAll(entities);
    }
    public void saveAll(List<Account> entities) {
        dao.saveAll(entities);
    }
    public ArrayList<Account> findAll() {
        return dao.findAll();
    }
    public boolean deleteById(Long id) {
        return dao.deleteById(id);
    }
    public Account getOne(Long id) {
        return dao.getOne(id);
    }

    public Long getIdByNumber(String number){
        Long id = dao.findAll().stream().filter(x -> Objects.equals(x.number, number)).findFirst().get().getId();
        if (id == null) return 0L;
        else return id;
    }
    public void deposit(Long id, Double amount){
        Account acc = dao.getOne(id);
        acc.setBalance(acc.getBalance()+amount);
    }
    public boolean withdraw(Long id, Double amount){
        Account acc = dao.getOne(id);
        if(acc.getBalance()>=amount){
            acc.setBalance(acc.getBalance()-amount);
            return true;
        }
        else return false;
    }
    public boolean transfer(Long from, Long to, Double amount){
        if(dao.getOne(from).getBalance()>=amount){
            withdraw(from, amount);
            deposit(to, amount);
            return true;
        }
        else return false;
    }
}
