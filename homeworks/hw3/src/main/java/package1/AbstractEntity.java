package package1;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    Long id;
    @Column
    @CreatedDate
    Date createdDate;
    @Column
    @LastModifiedDate
    Date lastModifiedDate;
}
