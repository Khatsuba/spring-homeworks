package package1;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CustomerService{
    private final CustomerDAO dao;
    private final AccountService accountService;
    private final EmployerService employerService;

    public Customer save(Customer obj) {
        return dao.save(obj);
    }

    public boolean delete(Customer obj) {
        return dao.delete(obj);
    }


    public void deleteAll(ArrayList<Customer> entities) {
        dao.deleteAll(entities);
    }


    public void saveAll(List<Customer> entities) {
        dao.saveAll(entities);
    }


    public ArrayList<Customer> findAll() {
        return dao.findAll();
    }


    public boolean deleteById(Long id) {
        return dao.deleteById(id);
    }

    public Customer getOne(Long id) {
        return dao.getOne(id);
    }

    public Customer editById(Long id, String name, String email, Integer age){
        dao.getOne(id).setAge(age);
        dao.getOne(id).setName(name);
        dao.getOne(id).setEmail(email);
        return dao.getOne(id);
    }

    public Customer addAcc(Long id) {
        Customer customer = dao.getOne(id);

        if (customer!=null) {
            Account newAccount = accountService.save(new Account(Currency.UAH, customer)); // UAH???
            customer.getAccounts().add(newAccount);

            return dao.save(customer);
        } else {
            return null;
        }
    }

    public boolean deleteAcc(Long cid, Long aid){
        Customer customer = dao.getOne(cid);

        if (customer!=null) {
            if (customer.getAccounts()!=null)
                customer.getAccounts().stream()
                        .filter(x -> x.getId().equals(aid))
                        .findFirst()
                        .ifPresent(x ->{
                                    customer.getAccounts().remove(x);
                                    accountService.delete(x);
                                }
                        );
            return true;
        } else {
            return false;
        }
    }

    public Customer addEmployer(Long cid, Long eid) {
        Customer customer = dao.getOne(cid);
        Employer employer = employerService.getOne(eid);

        if (customer!=null && !customer.getEmployers().contains(employer)) {
            customer.getEmployers().add(employer);
            return dao.save(customer);
        } else {
            return null;
        }
    }

    public boolean deleteEmployer(Long cid, Long eid){
        Customer customer = dao.getOne(cid);
        Employer employer = employerService.getOne(eid);

        if (customer!=null) {
            if (customer.getEmployers()!=null)
                customer.getEmployers().stream()
                        .filter(x -> x.equals(employer))
                        .findFirst()
                        .ifPresent(x ->{
                                    customer.getEmployers().remove(x);
                                }
                        );
            return true;
        } else {
            return false;
        }
    }
}
