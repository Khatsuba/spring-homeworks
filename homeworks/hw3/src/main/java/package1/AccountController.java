package package1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {
    private final AccountFacade accFacade;

    @PostMapping("/deposit")
    public ResponseEntity<Object> deposit(@RequestParam Long from, @RequestParam Double amount){
        System.out.println(from.toString());
        accFacade.deposit(from, amount);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + accFacade.getOne(from).getCustomer().getId())
                .build();
    }
    @PostMapping("/withdraw")
    public ResponseEntity<Object> withdraw(@RequestParam Long from, @RequestParam Double amount){
        accFacade.withdraw(from, amount);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + accFacade.getOne(from).getCustomer().getId())
                .build();
    }
    @PostMapping("/transfer")
    public ResponseEntity<Object> transfer(@RequestParam Long from, @RequestParam String to, @RequestParam Double amount){
        Long other = accFacade.getIdByNumber(to);
        if (other!=0L){
            accFacade.transfer(from, other, amount);
        }
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + accFacade.getOne(from).getCustomer().getId())
                .build();
    }

}
