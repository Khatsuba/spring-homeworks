package package1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
@Slf4j
public class EmployerController {
    private final EmployerFacade employerFacade;

    @GetMapping
    public ModelAndView viewAllEmployers() {
        log.info("Fetching all employers");
        ArrayList<EmployerResponse> employers = employerFacade.findAll();
        ModelAndView modelAndView = new ModelAndView("employers");
        modelAndView.addObject("employers", employers);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView editEmployerGet(@PathVariable("id") Long id) {
        log.info("Fetching employer with id: {}", id);
        EmployerResponse employer = employerFacade.getOne(id);
        ModelAndView modelAndView = new ModelAndView("employer-form");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView newEmployer() {
        log.info("Creating new employer form");
        EmployerRequest employer = new EmployerRequest();
        ModelAndView modelAndView = new ModelAndView("new-employer");
        modelAndView.addObject("employer", employer);
        return modelAndView;
    }

    @PostMapping("/new")
    public ResponseEntity<Object> save(@Validated @ModelAttribute EmployerRequest employer) {
        log.info("Saving new employer: {}", employer);
        employerFacade.save(employer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<Object> editEmployerPost(@PathVariable("id") Long id, @Validated @ModelAttribute EmployerRequest employerRequest) {
        log.info("Updating employer with id: {} with details: {}", id, employerRequest);
        employerFacade.updateEmployer(id, employerRequest);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
        log.info("Deleting employer with id: {}", id);
        employerFacade.deleteById(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/employers")
                .build();
    }
}
