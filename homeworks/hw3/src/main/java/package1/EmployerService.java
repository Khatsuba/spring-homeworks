package package1;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class EmployerService {
    private final EmployerDAO dao;

    public Employer save(Employer obj) {
        return dao.save(obj);
    }

    public boolean delete(Employer obj) {
        return dao.delete(obj);
    }


    public void deleteAll(ArrayList<Employer> entities) {
        dao.deleteAll(entities);
    }


    public void saveAll(List<Employer> entities) {
        dao.saveAll(entities);
    }


    public ArrayList<Employer> findAll() {
        return dao.findAll();
    }


    public boolean deleteById(Long id) {
        return dao.deleteById(id);
    }

    public Employer getOne(Long id) {
        return dao.getOne(id);
    }

    public Employer editById(Long id, String name, String address){
        dao.getOne(id).setName(name);
        dao.getOne(id).setAddress(address);
        return dao.getOne(id);
    }
}
