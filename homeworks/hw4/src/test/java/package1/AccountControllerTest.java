package package1;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountFacade accFacade;

    ModelMapper mapper = new ModelMapper();

    @Test
    public void testDeposit() throws Exception {
        Long from = 2L;
        Double amount = 100.0;
        Long customerId = 1L;

        Customer customer = new Customer("name", "email", 25);
        customer.setId(1L);
        Account account = new Account(Currency.UAH, customer);
        account.setId(2L);

        AccountResponse accResponse = mapper.map(account, AccountResponse.class);

        when(accFacade.getOne(from)).thenReturn(accResponse);

        // Perform the request and verify the response
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts/deposit")
                        .param("from", from.toString())
                        .param("amount", amount.toString()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/customers/" + customerId));

        // Verify that the facade methods were called with the expected arguments
        verify(accFacade, times(1)).deposit(from, amount);
        verify(accFacade, times(1)).getOne(from);
    }


    @Test
    public void testWithdraw() throws Exception {
        Long from = 2L;
        Double amount = 100.0;
        Long customerId = 1L;

        Customer customer = new Customer("name", "email", 25);
        customer.setId(1L);
        Account account = new Account(Currency.UAH, customer);
        account.setId(2L);

        AccountResponse accResponse = mapper.map(account, AccountResponse.class);

        when(accFacade.getOne(from)).thenReturn(accResponse);

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts/withdraw")
                        .param("from", from.toString())
                        .param("amount", amount.toString()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/customers/" + customerId));

        verify(accFacade, times(1)).withdraw(from, amount);
        verify(accFacade, times(1)).getOne(from);
    }

    @Test
    public void testTransfer() throws Exception {
        String to = "account number";
        Long otherId = 3L;

        Long from = 2L;
        Double amount = 100.0;
        Long customerId = 1L;

        Customer customer = new Customer("name", "email", 25);
        customer.setId(1L);
        Account account = new Account(Currency.UAH, customer);
        account.setId(2L);

        AccountResponse accResponse = mapper.map(account, AccountResponse.class);

        when(accFacade.getOne(from)).thenReturn(accResponse);
        when(accFacade.getIdByNumber(to)).thenReturn(otherId);

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts/transfer")
                        .param("from", from.toString())
                        .param("to", to)
                        .param("amount", amount.toString()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, "/customers/" + customerId));

        verify(accFacade, times(1)).transfer(from, otherId, amount);
        verify(accFacade, times(1)).getOne(from);
        verify(accFacade, times(1)).getIdByNumber(to);
    }
}