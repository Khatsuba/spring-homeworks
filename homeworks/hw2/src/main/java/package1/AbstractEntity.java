package package1;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractEntity {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    Long id;
}
