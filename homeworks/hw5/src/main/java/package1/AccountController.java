package package1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@Slf4j
public class AccountController {
    private final AccountFacade accFacade;

    @Autowired
    private AccountWebSocketHandler webSocketHandler;

    @PostMapping("/deposit")
    public ResponseEntity<Map<String, Object>> deposit(@RequestParam Long from, @RequestParam Double amount) {
        log.info("Deposit request received: from={}, amount={}", from, amount);
        accFacade.deposit(from, amount);
        String message = "Депозит виконано для " + from + ". Баланс: " + accFacade.getOne(from).getBalance();
        webSocketHandler.sendMessageToClients(message);

        Map<String, Object> response = new HashMap<>();
        response.put("status", "success");
        response.put("message", message);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/withdraw")
    public ResponseEntity<Map<String, Object>> withdraw(@RequestParam Long from, @RequestParam Double amount) {
        log.info("Withdraw request received: from={}, amount={}", from, amount);
        String message;
        if (accFacade.withdraw(from, amount)){
            message = "Зняття коштів виконано з " + from + ". Баланс: " + accFacade.getOne(from).getBalance();
        }else{
            message = "Зняття коштів з " + from + " не вдалося. Баланс: " + accFacade.getOne(from).getBalance();
        }
        webSocketHandler.sendMessageToClients(message);

        Map<String, Object> response = new HashMap<>();
        response.put("status", "success");
        response.put("message", message);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/transfer")
    public ResponseEntity<Map<String, Object>> transfer(@RequestParam Long from, @RequestParam String to, @RequestParam Double amount) {
        log.info("Transfer request received: from={}, to={}, amount={}", from, to, amount);
        Long other = accFacade.getIdByNumber(to);
        if (other != 0L) {
            String message;
            if (accFacade.transfer(from, other, amount)){
                message = "Передача коштів з " + from + " на акаунт " + other + ". Сума: " + amount;
            }else{
                message = "Передача коштів з " + from + " на акаунт " + other + " не вдалася";
            }
            webSocketHandler.sendMessageToClients(message);

            Map<String, Object> response = new HashMap<>();
            response.put("status", "success");
            response.put("message", message);
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.badRequest().body(Map.of("status", "error", "message", "Invalid account number"));
        }
    }
}
