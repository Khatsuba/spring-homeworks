package package1;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Entity
@Table
@NoArgsConstructor
public class Account extends AbstractEntity {
    @Column
    String number;
    @Column
    Currency currency;
    @Column
    Double balance;
    @ManyToOne
    Customer customer;

    Account(Currency currency, Customer customer){
        this.number = UUID.randomUUID().toString();
        this.currency = currency;
        this.balance = 0.0;
        this.customer = customer;
    }
}
