package package1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CustomerServiceTest {

    @MockBean
    private CustomerDAO customerDAO;

    @MockBean
    private AccountService accountService;

    @MockBean
    private EmployerService employerService;

    @Autowired
    private CustomerService customerService;

    @Test
    public void testSave() {
        Customer customer = new Customer();
        when(customerDAO.save(customer)).thenReturn(customer);

        Customer result = customerService.save(customer);

        verify(customerDAO, times(1)).save(customer);
        assertEquals(customer, result);
    }

    @Test
    public void testDelete() {
        Customer customer = new Customer();
        when(customerDAO.delete(customer)).thenReturn(true);

        boolean result = customerService.delete(customer);

        verify(customerDAO, times(1)).delete(customer);
        assertTrue(result);
    }

    @Test
    public void testFindAll() {
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(2L);
        ArrayList<Customer> customers = new ArrayList<>(Arrays.asList(customer1, customer2));

        when(customerDAO.findAll()).thenReturn(customers);

        ArrayList<Customer> result = customerService.findAll();

        verify(customerDAO, times(1)).findAll();
        assertEquals(2, result.size());
        assertTrue(result.contains(customer1));
        assertTrue(result.contains(customer2));
    }

    @Test
    public void testFindAllWithPageable() {
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(2L);
        ArrayList<Customer> customers = new ArrayList<>(Arrays.asList(customer1, customer2));

        Pageable pageable = PageRequest.of(0, 10);
        Page<Customer> customerPage = new PageImpl<>(customers, pageable, customers.size());

        when(customerDAO.findAll(pageable)).thenReturn(customerPage);

        Page<Customer> result = customerService.findAll(pageable);

        verify(customerDAO, times(1)).findAll(pageable);
        assertEquals(2, result.getTotalElements());
        assertEquals(1L, result.getContent().get(0).getId());
        assertEquals(2L, result.getContent().get(1).getId());
    }

    @Test
    public void testDeleteById() {
        when(customerDAO.deleteById(1L)).thenReturn(true);

        boolean result = customerService.deleteById(1L);

        assertTrue(result);
        verify(customerDAO, times(1)).deleteById(1L);
    }

    @Test
    public void testGetOne() {
        Customer customer = new Customer();

        when(customerDAO.getOne(1L)).thenReturn(customer);

        Customer result = customerService.getOne(1L);

        assertEquals(result, customer);
    }

    @Test
    public void testEditById() {
        Customer customer = new Customer();

        when(customerDAO.getOne(1L)).thenReturn(customer);

        customerService.editById(1L, "name", "email", 25, "+38 067 789 2444");

        verify(customerDAO, times(5)).getOne(1L);
        assertEquals(customer.getAge(), 25);
        assertEquals(customer.getName(), "name");
        assertEquals(customer.getEmail(), "email");
        assertEquals(customer.getPhone(), "+38 067 789 2444");
    }

    @Test
    public void testAddAcc() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setAccounts(new ArrayList<>());

        Account account = new Account(Currency.UAH, customer);
        account.setId(2L);

        when(customerDAO.getOne(1L)).thenReturn(customer);
        when(accountService.save(any(Account.class))).thenReturn(account);
        when(customerDAO.save(customer)).thenReturn(customer);

        Customer result = customerService.addAcc(1L);

        assertEquals(customer, result);
        assertEquals(1, customer.getAccounts().size());
        assertEquals(account, customer.getAccounts().get(0));
        verify(accountService, times(1)).save(any(Account.class));
        verify(customerDAO, times(1)).save(customer);
    }

    @Test
    public void testDeleteAcc() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setAccounts(new ArrayList<>());

        Account account = new Account(Currency.UAH, customer);
        account.setId(2L);
        customer.getAccounts().add(account);

        when(customerDAO.getOne(1L)).thenReturn(customer);
        when(accountService.delete(account)).thenReturn(true);

        boolean result = customerService.deleteAcc(1L, 2L);

        assertTrue(result);
        assertEquals(0, customer.getAccounts().size());
        verify(accountService, times(1)).delete(any(Account.class));
    }

    @Test
    public void testAddEmployer() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setEmployers(new ArrayList<>());

        Employer employer = new Employer();
        employer.setId(2L);

        when(customerDAO.getOne(1L)).thenReturn(customer);
        when(employerService.getOne(2L)).thenReturn(employer);
        when(customerDAO.save(customer)).thenReturn(customer);

        Customer result = customerService.addEmployer(1L, 2L);

        assertEquals(customer, result);
        assertEquals(1, customer.getEmployers().size());
        assertEquals(employer, customer.getEmployers().get(0));
        verify(employerService, times(1)).getOne(2L);
        verify(customerDAO, times(1)).save(customer);
    }

    @Test
    public void testDeleteEmployer() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setEmployers(new ArrayList<>());

        Employer employer = new Employer();
        employer.setId(2L);
        customer.getEmployers().add(employer);

        when(customerDAO.getOne(1L)).thenReturn(customer);
        when(employerService.getOne(2L)).thenReturn(employer);

        boolean result = customerService.deleteEmployer(1L, 2L);

        assertTrue(result);
        assertEquals(0, customer.getEmployers().size());
        verify(employerService, times(1)).getOne(2L);
    }

    @Test
    public void testClearEmployers() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setEmployers(new ArrayList<>());

        Employer employer = new Employer();
        employer.setId(2L);

        when(customerDAO.getOne(1L)).thenReturn(customer);

        customerService.clearEmployers(1L);

        assertEquals(0, customer.getEmployers().size());
        verify(customerDAO, times(1)).getOne(1L);
    }
}