package package1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableJpaAuditing
@SpringBootApplication()
public class Application {

    public static void main(String[] args) {
        String rawPassword = "102030";
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(rawPassword);
        System.out.println("Password for bogdan.kovalenko@example.com");
        System.out.println(rawPassword + " encoded as " + encodedPassword);
        System.out.println("localhost:9000/customers");

        SpringApplication.run(Application.class, args);
    }


}
