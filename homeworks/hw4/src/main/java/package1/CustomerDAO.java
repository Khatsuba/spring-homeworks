package package1;


import jakarta.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import package1.Customer;
import package1.DAO;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDAO implements DAO<Customer> {
    private final CustomerRepository repository;

    public CustomerDAO(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Customer save(Customer obj) {
        repository.save(obj);
        return obj;
    }

    @Override
    public boolean delete(Customer obj) {
        if(repository.findAll().contains(obj)) {
            repository.delete(obj);
            return true;
        }
        else return false;
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        repository.deleteAll();
    }

    @Override
    public void saveAll(List<Customer> entities) {
        repository.saveAll(entities);
    }

    @Override
    public ArrayList<Customer> findAll(){
        return (ArrayList<Customer>) repository.findAll();
    }

    public Page<Customer> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public boolean deleteById(Long id) {
        repository.deleteById(id);
        return true;
    }

    @Override
    public Customer getOne(Long id) {
        return repository.findById(id).orElse(null);
    }
}
