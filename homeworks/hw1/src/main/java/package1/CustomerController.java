package package1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final AccountService accountService;

    //GetMappings

    @GetMapping
    public ModelAndView viewAllCustomers() {
        ArrayList<Customer> customers = customerService.findAll();
        ModelAndView modelAndView = new ModelAndView("customers");
        modelAndView.addObject("customers", customers);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView viewCustomer(@PathVariable("id") Long id) {
        Customer customer = customerService.getOne(id);
        Account accounts = accountService.getOne(id);
        ModelAndView modelAndView = new ModelAndView("customer-details");
        modelAndView.addObject("customer", customer);
        modelAndView.addObject("accounts", accounts);
        return modelAndView;
    }

    @GetMapping("/{cid}/accounts/{aid}")
    public ModelAndView viewAccount(@PathVariable("aid") Long aid) {
        Account account = accountService.getOne(aid);
        ModelAndView modelAndView = new ModelAndView("account-details");
        modelAndView.addObject("account", account);
        modelAndView.addObject("customer", account.getCustomer());
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView showCreateCustomerForm() {
        ModelAndView modelAndView = new ModelAndView("new-customer");
        modelAndView.addObject("customer", new Customer());
        return modelAndView;
    }

    @GetMapping("/{id}/edit")
    public ModelAndView editCustomerForm(@PathVariable("id") Long id) {
        Customer customer = customerService.getOne(id);
        ModelAndView modelAndView = new ModelAndView("customer-form");
        modelAndView.addObject("customer", customer);
        return modelAndView;
    }

    //PostMappings

    @PostMapping("/new")
    public ResponseEntity<Object> save(@ModelAttribute Customer customer) {
        customerService.save(customer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers")
                .build();
    }

    @PostMapping("/{id}")
    public ResponseEntity<Object> addAccount(@PathVariable("id") Long id) {
        customerService.addAcc(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + id)
                .build();
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @RequestParam String name, @RequestParam String email, @RequestParam Integer age) {
        Customer customer = customerService.getOne(id);
        customer.setName(name);
        customer.setEmail(email);
        customer.setAge(age);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + id)
                .build();
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
        customerService.deleteById(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers")
                .build();
    }

    @PostMapping("/{cid}/accounts/{aid}/delete")
    public ResponseEntity<Object> deleteAccount(@PathVariable("cid") Long cid, @PathVariable("aid") Long aid){
        customerService.deleteAcc(cid, aid);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + cid)
                .build();
    }
}
