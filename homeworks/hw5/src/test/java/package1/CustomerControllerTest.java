package package1;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class CustomerControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private AccountFacade accountFacade;

    @Mock
    private EmployerFacade employerFacade;

    @InjectMocks
    private CustomerController customerController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }

    @Test
    void testViewAllCustomers() throws Exception {
        // Setup mock data
        CustomerResponse customer1 = new CustomerResponse();
        CustomerResponse customer2 = new CustomerResponse();
        Page<CustomerResponse> page = new PageImpl<>(Arrays.asList(customer1, customer2), PageRequest.of(0, 10), 1);

        when(customerFacade.findAll(anyInt(), anyInt())).thenReturn(page);

        mockMvc.perform(get("/customers/")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())  // Assert the status is 200 OK
                .andExpect(view().name("customers"))  // Assert the view name
                .andExpect(model().attribute("customers", page.getContent()))  // Assert the model attributes
                .andExpect(model().attribute("totalPages", page.getTotalPages()))
                .andExpect(model().attribute("currentPage", 0));
    }


    @Test
    void testViewCustomer() throws Exception {
        Long id = 1L;
        CustomerResponse customerResponse = new CustomerResponse();
        List<EmployerResponse> employers = List.of(new EmployerResponse());
        when(customerFacade.getOne(eq(id))).thenReturn(customerResponse);
        when(customerFacade.getEmployers(eq(id))).thenReturn(employers);

        mockMvc.perform(get("/customers/{id}", id))
                .andExpect(status().isOk())
                .andExpect(view().name("customer-details"))
                .andExpect(model().attribute("customer", customerResponse))
                .andExpect(model().attribute("employers", employers));
    }

    @Test
    void testViewAccount() throws Exception {
        Long aid = 2L;
        AccountResponse account = new AccountResponse();
        account.setCustomer(new CustomerResponse());
        when(accountFacade.getOne(aid)).thenReturn(account);

        mockMvc.perform(get("/customers/{cid}/accounts/{aid}", 1L, aid))
                .andExpect(status().isOk())
                .andExpect(view().name("account-details"))
                .andExpect(model().attributeExists("account", "customer"));
    }

    @Test
    void testShowCreateCustomerForm() throws Exception {
        mockMvc.perform(get("/customers/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("new-customer"))
                .andExpect(model().attributeExists("customer"));
    }

    @Test
    void testEditCustomerForm() throws Exception {
        Long id = 1L;
        CustomerResponse customer = new CustomerResponse();
        ArrayList<EmployerResponse> employers = new ArrayList<>(List.of(new EmployerResponse()));
        ArrayList<EmployerResponse> allEmployers = new ArrayList<>(List.of(new EmployerResponse()));
        when(customerFacade.getOne(id)).thenReturn(customer);
        when(customerFacade.getEmployers(id)).thenReturn(employers);
        when(employerFacade.findAll()).thenReturn(allEmployers);

        mockMvc.perform(get("/customers/{id}/edit", id))
                .andExpect(status().isOk())
                .andExpect(view().name("customer-form"))
                .andExpect(model().attributeExists("customer", "employers", "allEmployers"));
    }

    @Test
    void testSaveCustomer() throws Exception {
        mockMvc.perform(post("/customers/new")
                        .param("name", "Name")
                        .param("age", "30")
                        .param("email", "Email1@gmail.com")
                        .param("phone", "+38 067 844 2334"))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string(HttpHeaders.LOCATION, "/customers"));

        verify(customerFacade, times(1)).save(any(CustomerRequest.class));
    }

    @Test
    void testAddAccount() throws Exception {
        Long id = 1L;

        mockMvc.perform(post("/customers/{id}", id))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string(HttpHeaders.LOCATION, "/customers/" + id));

        verify(customerFacade, times(1)).addAcc(id);
    }

    @Test
    void testEditCustomer() throws Exception {
        Long id = 1L;

        CustomerRequest customerRequest = new CustomerRequest();
        customerRequest.setName("Name");
        customerRequest.setAge(30);
        customerRequest.setEmail("Email1@gmail.com");
        customerRequest.setPhone("+38 067 844 2334");

        mockMvc.perform(post("/customers/{id}/edit", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(customerRequest)))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string(HttpHeaders.LOCATION, "/customers/" + id));

        verify(customerFacade, times(1)).updateCustomer(eq(id), any(CustomerRequest.class));
    }

    @Test
    void testDeleteById() throws Exception {
        Long id = 1L;

        mockMvc.perform(post("/customers/{id}/delete", id))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string(HttpHeaders.LOCATION, "/customers"));

        verify(customerFacade, times(1)).deleteById(id);
    }

    @Test
    void testDeleteAccount() throws Exception {
        Long cid = 1L;
        Long aid = 1L;

        mockMvc.perform(post("/customers/{cid}/accounts/{aid}/delete", cid, aid))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string(HttpHeaders.LOCATION, "/customers/" + cid));

        verify(customerFacade, times(1)).deleteAcc(cid, aid);
    }
}
