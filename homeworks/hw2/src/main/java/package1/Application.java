package package1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication()
public class Application {

    public static void main(String[] args) {
        System.out.println("localhost:9000/customers");
        SpringApplication.run(Application.class, args);
    }
}
