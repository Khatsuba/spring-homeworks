package package1;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class AccountRequest {
    Long from;
    Long to;
    @Min(value = 0)
    Double amount;
}
