package package1;

import lombok.Data;

@Data
public class AccountResponse {
    Long id;
    String number;
    String currency;
    Double balance;
    CustomerResponse customer;
}
