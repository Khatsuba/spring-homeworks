package package1;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {
    private final CustomerFacade customerFacade;
    private final AccountFacade accountFacade;
    private final EmployerFacade employerFacade;

    // GetMappings

    @GetMapping()
    public ModelAndView viewAllCustomers(@RequestParam(defaultValue = "0") int page,
                                         @RequestParam(defaultValue = "10") int size) {
        log.info("Fetching customers - page: {}, size: {}", page, size);
        Page<CustomerResponse> customers = customerFacade.findAll(page, size);
        ModelAndView modelAndView = new ModelAndView("customers");
        modelAndView.addObject("customers", customers.getContent());
        modelAndView.addObject("totalPages", customers.getTotalPages());
        modelAndView.addObject("currentPage", page);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView viewCustomer(@PathVariable("id") Long id) {
        log.info("Fetching customer with id: {}", id);
        CustomerResponse customerResponse = customerFacade.getOne(id);
        ModelAndView modelAndView = new ModelAndView("customer-details");
        modelAndView.addObject("customer", customerResponse);
        modelAndView.addObject("employers", customerFacade.getEmployers(id));
        return modelAndView;
    }

    @GetMapping("/{cid}/accounts/{aid}")
    public ModelAndView viewAccount(@PathVariable("aid") Long aid) {
        log.info("Fetching account with id: {}", aid);
        AccountResponse account = accountFacade.getOne(aid);
        ModelAndView modelAndView = new ModelAndView("account-details");
        modelAndView.addObject("account", account);
        modelAndView.addObject("customer", account.getCustomer());
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView showCreateCustomerForm() {
        log.info("Showing new customer form");
        ModelAndView modelAndView = new ModelAndView("new-customer");
        modelAndView.addObject("customer", new CustomerRequest());
        return modelAndView;
    }

    @GetMapping("/{id}/edit")
    public ModelAndView editCustomerForm(@PathVariable("id") Long id) {
        log.info("Showing edit form for customer with id: {}", id);
        CustomerResponse customer = customerFacade.getOne(id);
        List<EmployerResponse> employers = customerFacade.getEmployers(id);
        List<EmployerResponse> allEmployers = employerFacade.findAll();
        ModelAndView modelAndView = new ModelAndView("customer-form");
        modelAndView.addObject("customer", customer);
        modelAndView.addObject("employers", employers);
        modelAndView.addObject("allEmployers", allEmployers);
        return modelAndView;
    }

    // PostMappings

    @PostMapping("/new")
    public ResponseEntity<Object> save(@Validated @ModelAttribute CustomerRequest customer) {
        log.info("Saving new customer: {}", customer);
        customerFacade.save(customer);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers")
                .build();
    }

    @PostMapping("/{id}")
    public ResponseEntity<Object> addAccount(@PathVariable("id") Long id) {
        log.info("Adding account to customer with id: {}", id);
        customerFacade.addAcc(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + id)
                .build();
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @Validated @RequestBody CustomerRequest customerRequest) {
        log.info("Updating customer with id: {} with details: {}", id, customerRequest);
        customerFacade.updateCustomer(id, customerRequest);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + id)
                .build();
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
        log.info("Deleting customer with id: {}", id);
        customerFacade.deleteById(id);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers")
                .build();
    }

    @PostMapping("/{cid}/accounts/{aid}/delete")
    public ResponseEntity<Object> deleteAccount(@PathVariable("cid") Long cid, @PathVariable("aid") Long aid) {
        log.info("Deleting account with id: {} from customer with id: {}", aid, cid);
        customerFacade.deleteAcc(cid, aid);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, "/customers/" + cid)
                .build();
    }
}
