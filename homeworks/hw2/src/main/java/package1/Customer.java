package package1;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;

@Data
@Entity
@NoArgsConstructor
@Table
public class Customer extends AbstractEntity{
    @Column
    String name;
    @Column
    String email;
    @Column
    Integer age;
    @OneToMany(cascade = CascadeType.ALL)
    List<Account> accounts = new ArrayList<>();
    @ManyToMany
    @JoinTable(
            name = "customer_employers",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "employer_id")
    )
    List<Employer> employers = new ArrayList<>();

    Customer(String name, String email, Integer age){
        this.name = name;
        this.email = email;
        this.age = age;
    }

}
